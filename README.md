# Receptari íntim
Fa estona que prov de fer un receptari íntim. Vull que m'envieu ses vostres receptes amb una història (per què són importants per vosaltres, qui la vos va ensenyar, a qui vos recorda, a quin temps vos transporta, etc).

Vull fer un git públic i/o un ebook gratuït en tenir-ne a bastament.
Enviau-les per [Mastodon](https://mastodont.cat/deck/tags/SaMevaRecepta), per privat, per email o cmo [àudio](https://www.speakpipe.com/SaMevaRecepta) (especificau si voleu que siguin anònimes, no voleu que s'història sigui pública o si me l'enviau només pes meu gaudi privat)

Gràcies!
