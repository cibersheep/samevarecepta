# Coca Magdalena
## Dificultat: 
  - **fàcil** (si sabeu fregir un ou)
  - **mèdia** (si sempre els heu bullit)
  - **difícil** (si menjau de congelats)

## Ingredients:
  - ous (depèn de la mida, nosaltres feim servir entre 12 i 18 i és que les nostres gallines són petit. Es pot fer amb manco)
  - 1 tassa de farina + una mica més (pes motllo)
  - 1 tassa de sucre
  - Sucre en pols (opcional)
  - Saïm

## Preparació:
- Separau els blancs dels vermells en dos bols diferents.
- Empastifau el motllo amb una capa prima de saïm i empolvau amb la farina addicional. La idea és que hi quedi una capa perquè la coca no s'aferri (no em digueu que en feu servir de silicona)
- Pujau els blancs d'ou a punt de neu (fins que no lleneguin en tombar el bol). Aquesta és la part més important, així que feis-ho a conciència, que no vos faci vessa. De debò, feis-me cas que no voleu fer una truita dolça (llegiu s'història).
- Enceneu el forn perquè es comenci a encalentir.
- Remenau els vermells d'ou.
- Afegiu la tassa de sucre i la de farina als vermells lentament. Obtindreu una massa espessa.
- Afegiu la massa al blanc i mesclau-ho bé.
- Abocau la barreja dins el motllo fins més o manco a la meitat i enfornau a foc fort fins que pugi (es bufi).
- Quan hagi pujat, abaixau la temperatura del forn al mínim i vigileu-la fins que agafi bon color ([Pantone PMS-138-C](https://www.myperfectcolor.com/paint/379141-pantone-pms-138-c))
- Amb un escuradents comprovau si és cuit (ha de sortir net).
- Ara, no em creureu perquè ningú ho fa: just en treure la coca del forn (i si el motlle no és de goma ni de vidre, mem si ens entenem), deixau-la caure a terra. Alertau que no tombi, i no vos flipeu, que no sigui més d'un pam o pam i mig d'enterra. No sigueu ases.
Sona una tonteria i una mica ridícul, però farà que la coca es mantingui esponjosa [així](https://www.youtube.com/shorts/gojBFxPTZZs)
- Deixau que se refredi i emplatau.
- Si voleu, podeu afegir sucre en pols quan es refredi o abans de servir-la.

# I ara s'història:
Mai he estat bon cuiner quan s'ha de fer dolç i aquest és sa segona recepta que vaig aprendre a fer, encara que em costà fer una gran truita d'ous dolça perquè vaig fer de vago-maleante i no vaig pensar que muntar els ous per separat i bé fos tan important.
Aqueixa és una recepta molt especial perquè ve de la meva sogra a través des meu home. Sa mare tenia una manera molt simpàtica d'explicar les coses, un to pacient i una visió molt pragmàtica de la vida: «Idò» era una resposta al desconegut. Sabia totes les maneres tradicionals de fer les coses. Quan li vaig preguntar com es feia sa confitura, em va respondre: «Fes bullir sa fruita amb elsmateix pes de sucre que de fruita i deixa-la al sol durant quaranta dies».
Vos podreu imaginar aquesta recepta és una manera de recordar-la. Una d'encantadora.
